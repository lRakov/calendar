let Widgets;

(function (Widgets) {

  const EVENTS = [
    {
      startDate: "2022-01-02",
      endDate: "2022-01-05",
    },
    {
      startDate: "2022-01-04",
      endDate: "2022-01-20",
    },
    {
      startDate: "2022-01-10",
      endDate: "2022-01-19",
    },
    {
      startDate: "2022-01-12",
      endDate: "2022-01-25",
    },
    {
      startDate: "2022-01-26",
      endDate: "2022-02-10",
    },
  ];

  "use strict";

  const LOCALE = 'en-us';

  const CLS_ROOT = 'date-picker-root';
  const CLS_HEADER = 'date-picker-header';
  const CLS_PAYLOAD = 'date-picker-payload';
  const CLS_GRID = 'date-picker-grid';
  const CLS_CELL = 'date-picker-cell';
  const CLS_HEAD_CELL = 'date-picker-head-cell';
  const CLS_WEEKEND_CELL = 'date-picker-weekend-cell';
  const CLS_OUT_OF_RANGE_CELL = 'date-picker-out-of-range-cell';
  const CLS_BUTTON = 'date-picker-button';

  // language=CSS
  const STYLE = `
    .${CLS_ROOT} {
      display: flex;
      flex-direction: column;
      font-family: sans-serif;
      gap: 28px;
    }

    .${CLS_HEADER} {
      text-align: center;
      font-weight: bold;
      font-size: 22px;
    }

    .${CLS_PAYLOAD} {
      display: flex;
      flex-direction: row;
      align-items: center;
      gap: 18px;
      user-select: none;
    }

    .${CLS_GRID} {
      display: grid;
      grid-template-columns: repeat(7, 1fr);
      grid-template-rows: 50px repeat(5, 1fr);
      grid-gap: 0;
      width: 308px;
      height: 266px;
      padding: 10px;
      margin: 4px;
      border: solid silver 1px;
      border-radius: 6px;
      align-items: center;
      justify-content: center;
    }

    .${CLS_CELL} {
      font-size: 16px;
      width: 100%;
      height: 35px;
      line-height: 32px;
      text-align: center;
      vertical-align: middle;
    }

    .${CLS_HEAD_CELL} {
      border-color: silver;
      border-bottom-style: solid;
      border-bottom-width: 1px;
      height: 40px;
      line-height: 40px;
      font-weight: bold;
    }

    .${CLS_WEEKEND_CELL} {
      color: red;
    }

    .${CLS_OUT_OF_RANGE_CELL} {
      visibility: hidden;
    }

    .${CLS_BUTTON} {
      color: #ffffff;
      background-color: #cccccc;
      font-family: sans-serif;
      font-size: 32px;
      border-radius: 28px;
      padding: 14px 14px;
      width: 16px;
      height: 16px;
      line-height: 14px;
      text-align: center;
      text-decoration: none;
      cursor: pointer;
    }

    .${CLS_BUTTON}:hover {
      background-color: silver;
    }

    .${CLS_BUTTON}:active {
      position: relative;
      top: 1px;
    }
  `;

  class Task {
    /**
     * @param {Date} startDate
     * @param {Date} endDate
     * @param {number} [count=1]
     */
    constructor(startDate, endDate, count = 1) {
      this.startDate = startDate;
      this.endDate = endDate;
      this.count = count;
    }

    isEmpty() {
      return this.startDate > this.endDate;
    }

    belongsTo(date) {
      return this.startDate <= date && date <= this.endDate;
    }

    hasCollision(other) {
      return this.startDate < other.endDate && this.endDate > other.startDate;
    }
  }

  class TaskManager {
    /**
     * @type {[Task]}
     */
    tasks = []

    /**
     * @param {[]} tasks
     */
    constructor(tasks) {
      tasks.forEach(item => this.put(new Task(toDate(item.startDate), toDate(item.endDate))));
    }

    /**
     * Complicity: O(log n)
     *
     * @param {Date} date
     * @return {number}
     */
    getTaskCountFor(date) {
      if (this.tasks.length === 0) {
        return 0;
      }
      console.log(date) // TODO remove
      return this.doGetTaskCountFor(date, 0, this.tasks.length) || 0;
    }

    /**
     * @param {Date} date
     * @param {number} min
     * @param {number} max
     * @return {number|null}
     */
    doGetTaskCountFor(date, min, max) {
      const middle = min + Math.floor((max - min)/2);
      console.log(min, ":", middle, ":", max); // TODO remove
      const middleTask = this.tasks[middle];
      if (middleTask.belongsTo(date)) {
        return middleTask.count
      }
      if (date < middleTask.startDate) {
        if (min < middle) {
          const result = this.doGetTaskCountFor(date, min, middle);
          if (result) {
            return result;
          }
        }
      } else {
        if (middle + 1 < max) {
          const result = this.doGetTaskCountFor(date, middle + 1, max);
          if (result) {
            return result;
          }
        }
      }
      return null;
    }

    /**
     * @param {Task} task
     */
    put(task) {
      if (this.tasks.length === 0) {
        this.tasks.push(task);
        return;
      }
      for (let i = 0; i < this.tasks.length; i++) {
        const oldTask = this.tasks[i];
        if (task.hasCollision(oldTask)) {
          const dates = [oldTask.startDate, oldTask.endDate, task.startDate, task.endDate].sort((a, b) => a - b);
          this.tasks.splice(i, 1, ...([
            new Task(dates[0], addDays(dates[1], -1), oldTask.count),
            new Task(dates[1], dates[2], oldTask.count + 1),
          ].filter(it => !it.isEmpty())))
          task = new Task(addDays(dates[2], 1), dates[3], 1);
        }
        if (task.isEmpty()) {
          return;
        }
      }
      if (!task.isEmpty()) {
        if (task.endDate < this.tasks[0].startDate) {
          this.tasks.unshift(task);
        } else {
          this.tasks.push(task);
        }
      }
    }
  }

  class DatePickerWidget {
    /**
     * @param props
     * @param {string} props.containerId
     * @param {Date} [props.date]
     */
    constructor(props) {
      this.container = document.getElementById(props.containerId);
      this.date = getFirstMonthDayFor(props.date || new Date());
      this.taskManager = new TaskManager(EVENTS)
    }

    init() {
      this.container.appendChild(this.#generateWidget());
    }

    /*
     * @param {Date} date
     */
    #update(date) {
      this.date = getFirstMonthDayFor(date);
      removeChildren(this.container);
      this.container.appendChild(this.#generateWidget());
    }

    /**
     * @returns {HTMLDivElement}
     */
    #generateWidget() {
      const result = createElement({classes: CLS_ROOT});
      result.appendChild(DatePickerWidget.#generateHeader(this.date));
      const payload = result.appendChild(createElement({classes: CLS_PAYLOAD}));
      payload.appendChild(createElement({tagName: 'a', innerText: '«', classes: CLS_BUTTON}))
        .addEventListener("click", () => {
          this.#update(addMonths(this.date, -1));
        });
      payload.appendChild(DatePickerWidget.#generateGrid(this.date, this.taskManager));
      payload.appendChild(createElement({tagName: 'a', innerText: '»', classes: CLS_BUTTON}))
        .addEventListener("click", () => {
          this.#update(addMonths(this.date, 1));
        });
      return result
    }

    /**
     * @param {Date} date
     * @returns {HTMLDivElement}
     */
    static #generateHeader(date) {
      return createElement({
        classes: CLS_HEADER,
        innerText: date.toLocaleString(LOCALE, {year: "numeric", month: "long"})
      });
    }

    /**
     * @param {Date} date
     * @param {TaskManager} taskManager
     * @return {HTMLDivElement}
     */
    static #generateGrid(date, taskManager) {
      const result = createElement({classes: CLS_GRID});
      for (const day of listFirstWeekDays(date)) {
        result.appendChild(DatePickerWidget.#generateHeaderCell(day));
      }
      for (const day of listMonthDays(date)) {
        result.appendChild(DatePickerWidget.#generateDayCell(day, isTheSameMonth(date, day), taskManager.getTaskCountFor(day)));
      }
      return result;
    }

    /**
     * @param {Date} date
     * @returns {HTMLDivElement}
     */
    static #generateHeaderCell(date) {
      const result = createElement({
        classes: [CLS_CELL, CLS_HEAD_CELL],
        innerText: getWeekdayLabelFor(date)
      });
      if (isWeekend(date)) {
        result.classList.add(CLS_WEEKEND_CELL);
      }
      return result;
    }

    /**
     * @param {Date} date
     * @param {boolean} isVisible
     * @param {number} daysCount
     * @returns {HTMLDivElement}
     */
    static #generateDayCell(date, isVisible, daysCount) {
      const result = createElement({
        classes: CLS_CELL,
        innerText: `${date.getDate()} [${daysCount}]`,
      });
      if (isWeekend(date)) {
        result.classList.add(CLS_WEEKEND_CELL);
      }
      if (!isVisible) {
        result.classList.add(CLS_OUT_OF_RANGE_CELL)
      }
      return result;
    }
  }

  /**
   * Widget library API
   */
  class Api {

    constructor() {
      const style = document.createElement('style');
      style.innerHTML = STYLE;
      document.head.appendChild(style);
    }

    /**
     * @param {string} props.containerId
     * @param {Date} [props.date]
     * @return {DatePickerWidget}
     */
    datePicker(props) {
      const result = new DatePickerWidget(props);
      result.init();
      return result;
    }
  }

  /**
   * @param {Object} [props]
   * @param {string} [props.tagName='div'] name of the tag
   * @param {string|string[]} [props.classes] css classes
   * @param {string} [props.innerText]
   * @returns {HTMLDivElement}
   */
  function createElement(props) {
    const result = document.createElement(props && props.tagName || 'div');
    if (props) {
      if (typeof props.classes === 'string') {
        result.classList.add(props.classes);
      } else if (Array.isArray(props.classes)) {
        result.classList.add(...props.classes)
      }
      props.innerText && (result.innerText = props.innerText);
    }
    return result;
  }

  function removeChildren(element) {
    while (element.firstChild) {
      element.removeChild(element.lastChild);
    }
  }

  /**
   * @param {Date} dateA
   * @param {Date} dateB
   * @return {boolean}
   */
  function isTheSameMonth(dateA, dateB) {
    return dateA.getFullYear() === dateB.getFullYear()
      && dateA.getMonth() === dateB.getMonth();
  }

  /**
   * @param {Date} date
   * @return {boolean}
   */
  function isWeekend(date) {
    return date.getDay() === 0
      || date.getDay() === 6
  }

  /**
   * @yields {Date}
   * @param {Date} date
   */
  function listFirstWeekDays(date) {
    return listDays(date, 7);
  }

  /**
   * @yields {Date}
   * @param {Date} date
   */
  function listMonthDays(date) {
    const firstDay = getFirstMonthDayFor(date);
    const lastDay = getLastMonthDayFor(date);
    const limit = firstDay.getDay()               // add days from the previous month
      + (lastDay.getDate() - firstDay.getDate())  // add days from the current month
      + (7 - lastDay.getDay());                   // add days from the next month
    return listDays(date, limit);
  }

  /**
   * @param {String} dateString
   * @return {Date}
   */
  function toDate(dateString) {
    const date = new Date(dateString);
    return new Date(date.getFullYear(), date.getMonth(), date.getDate());
  }

  /**
   * @yields {Date}
   * @param {Date} date
   * @param {number} limit
   */
  function* listDays(date, limit) {
    const firstMonthDay = getFirstMonthDayFor(date);
    let day = addDays(firstMonthDay, -firstMonthDay.getDay() - 1);
    while (limit-- > 0) {
      day = addDays(day, 1);
      yield day;
    }
  }

  /**
   * @param {Date} date
   * @param {number} dayNumber
   * @return {Date}
   */
  function addDays(date, dayNumber) {
    const result = new Date(date);
    result.setDate(result.getDate() + dayNumber);
    return result;
  }

  /**
   * @param {Date} date
   * @param {number} monthNumber
   * @return {Date}
   */
  function addMonths(date, monthNumber) {
    return new Date(date.setMonth(date.getMonth() + monthNumber));
  }

  /**
   * @param {Date} date
   * @return {Date}
   */
  function getFirstMonthDayFor(date) {
    return new Date(date.getFullYear(), date.getMonth(), 1);
  }

  /**
   * @param {Date} date
   * @return {Date}
   */
  function getLastMonthDayFor(date) {
    return new Date(date.getFullYear(), date.getMonth() + 1, 0);
  }

  /**
   * @param {Date} date
   * @param {string | string[]} [locales]
   * @return {string}
   */
  function getWeekdayLabelFor(date, locales = LOCALE) {
    return date
      .toLocaleString(locales, {weekday: "short"})
      .substring(0, 1);
  }

  Widgets.Api = Api;
})(Widgets || (Widgets = {}));

/**
 * Widget Api initialization
 */
if (typeof window['widgetsApi'] === 'undefined') {
  window.widgetsApi = new Widgets.Api();
}
