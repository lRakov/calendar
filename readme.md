# Notes

## Ways to improve

> Anything that you're not happy with, or you'd want to change or improve if you had unlimited time to work on this.

- There are no tests/quality check at all. So need to add them.
- Add an ability to customize widget(i18n, presentation, behavior)
- Check if it works on different browsers/platforms and fix such issues
- Split ./js/datePickerWidget.js on a few parts(files): Api class, class DatePickerWidget, groups of utility functions
- Reuse DOM elements(instead of recreation on month switching) to reduce performance penalty
- Switch script loading process to an asynchronous mode.
- CI/CD process consists of deploy stage only. Need to improve it a lot.


## Approaches

> What kind of other approaches you might take (tools/technologies/etc) if you were building this app to be "production-ready" for real users

- Use libraries, frameworks(if it doesn't break any restrictions about code base size): react, mui, dayjs, etc..
- Use already existed solution(try to find decent one)
- Add monitoring/metrics

## Task implementation schema

![Task implementation schema](taskImplemenationSchema.png)
